::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::
::::     https://npp-user-manual.org/docs/config-files/#editing-configuration-files
::::     https://npp-user-manual.org/docs/preferences/#shortcut-mapper
::::     https://npp-user-manual.org/docs/editing/#other-editing-commands-and-shortcuts
::::
:::: Questo file "notepad++ExecCMD.cmd" 
:::: permette a Notepad++ di aprire una finestra cmd "(cmd /k Full_Path_To_FileFolder)"
:::: nella directory del file aperto.
::::
:::: Mettilo in una cartella inserita in %PATH%
:::: 
:::: Nel file di configurazione di Notepad++
:::: %appdata%\Notepad++\shortcuts.xml
:::: per tutti gli utenti
:::: %ProgramFiles%\Notepad++\shortcuts.xml
:::: nella sezione <UserDefinedCommands>
:::: aggiungere questa riga
:::: <Command name="Apri terminale cmd nella dir del file.." Ctrl="no" Alt="yes" Shift="yes" Key="90">cmd /k "full_path_to_this_file\notepad++ExecCMD.cmd" &quot;$(FULL_CURRENT_PATH)&quot;</Command>
::::
:::: il file notepad++OpenCmdHere.cmd
:::: lo fà per te.
::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::    OLD-DOS stupid scripting language
::::    OLD-DOS stupido linuaggio di script 
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

@echo off

if .%1==. (
	echo\
	echo\##################################################
	echo 	Questo file:
	echo  "%~f0"
	echo 	é usato da Notepad++ per aprire
	echo 	una finestra cmd "(cmd /k Full_Path_To_FileFolder)"
	echo 	nella directory del file aperto.
	echo\
	echo 	Mettilo in una cartella inserita in %%PATH%%
	echo\
	echo 	Nel file di configurazione di Notepad++ 
	echo "%appdata%\Notepad++\shortcuts.xml"
	echo 	nella sezione 
	echo ^<UserDefinedCommands^>
	echo .. .  .   .
	echo ^</UserDefinedCommands^>
	echo 	aggiungi questa riga:
	echo\"<Command name="Apri terminale cmd nella dir del file.." Ctrl="no" Alt="yes" Shift="yes" Key="90">cmd /k "%~f0" &quot;$(FULL_CURRENT_PATH)&quot;</Command>"
	echo\
	echo 	lo script "notepad++OpenCmdHere.cmd" 
	echo 	lo fà per te.
	echo\##################################################
	echo\
	exit /b 1
)

net session /list >nul 2>&1 && set "MY_USER_IS_ADMIN=%errorlevel%"
if defined MY_USER_IS_ADMIN set "MY_USER_IS=[Administrator]"
if not defined MY_USER_IS_ADMIN set "MY_USER_IS=[User]"

set "MY_FILE_PATH=X"
if exist %1 (
	set "MY_FILE_DRIVE=%~d1"
	set "MY_FILE_PATH=%~dp1"
	set "MY_FILE_NAME=%~nx1"
	set "MY_FILE_SIZE=%~z1"
	set "MY_FILE_DATE=%~t1"
) else (
	set MY_FILE_PATH=
)

if not defined MY_FILE_PATH (
	cd "%userprofile%"
	title %MY_USER_IS% %username% notepad^+^+  %date% %time%
	echo notepad^+^+
	echo %MY_USER_IS%-%username% %date%-%time%
	echo file non ancora salvato %1
)

if defined MY_FILE_PATH (
	%MY_FILE_DRIVE%
	cd "%MY_FILE_PATH%"
	title %MY_USER_IS% %username% notepad^+^+  "%MY_FILE_PATH%"  %date% %time%
	echo\notepad^+^+ Apre cmd nella cartella del file
	echo %MY_USER_IS%-%username% %date%-%time%
	echo "%MY_FILE_PATH%" 
	echo "%MY_FILE_NAME%" %MY_FILE_SIZE%^(byte^) modificato il %MY_FILE_DATE%
)

for /f "delims=^=" %%A in ('@set MY_') do @set "%%A="