::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:::: 
::::     https://npp-user-manual.org/docs/config-files/#editing-configuration-files
::::     https://npp-user-manual.org/docs/preferences/#shortcut-mapper
::::     https://npp-user-manual.org/docs/editing/#other-editing-commands-and-shortcuts
:::: 
::::    questo script inserisce la riga %SEARCH_THIS%
::::    SEARCH_THIS=<Command name="Apri terminale cmd nella dir del file.." Ctrl="no" Alt="yes" Shift="yes" Key="90">cmd /k cmdForNotepad++.cmd &quot;$(FULL_CURRENT_PATH)&quot;</Command>
::::    nel file %FILE_IN%
::::    FILE_IN=%appdata%\notepad++\shortcuts.xml
:::: 
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::    OLD-DOS stupid scripting language
::::    OLD-DOS stupido linuaggio di script 
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
@echo off
setlocal EnableDelayedExpansion
set START_TIME=%date% - %time%

echo \\\\\\\\\\\\\\\\\\\\\\\\\\
echo  %~f0  start %START_TIME%

set  "FILE_IN=%appdata%\notepad++\shortcuts.xml"
:::: FOR TEST
REM set  "FILE_IN=C:\Users\PARETO-User\Desktop\shortcuts.xml"

:::: FOR TEST
REM set "FILE_OUT=%appdata%\notepad++\shortcuts.xml"
REM set "FILE_OUT=C:\Users\PARETO-User\Desktop\shortcuts.xml"
set "FILE_OUT=%FILE_IN%"

if not exist "%FILE_IN%" (
	echo\
	echo 	"FILE_IN" %FILE_IN%
	echo 	 non c'é il file
	echo\
	goto :END_00
)

set /a COUNT=0
set /a N_IN=0
set "SEARCH_THIS=        <Command name="Apri terminale cmd nella dir del file.." Ctrl="no" Alt="yes" Shift="yes" Key="90">cmd /k notepad++ExecCMD.cmd &quot;$(FULL_CURRENT_PATH)&quot;</Command>"
set "SEARCH_Q=%SEARCH_THIS%"&    REM sostituisco i caratteri indesiderati
set "SEARCH_Q=%SEARCH_Q:<=§%"
set "SEARCH_Q=%SEARCH_Q:>=€%"
set "SEARCH_Q=%SEARCH_Q:"='%"
set "SEARCH_Q=%SEARCH_Q:&=#%"
set "SEARCH_Q=%SEARCH_Q:;=ç%"
set "SEARCH_Q=%SEARCH_Q:!=±%"

set "INSERT_BEFORE_THIS=    </UserDefinedCommands>"
set "INSERT_BEFORE_Q=%INSERT_BEFORE_THIS:<=§%"&    REM sostituisco i caratteri indesiderati
set "INSERT_BEFORE_Q=%INSERT_BEFORE_Q:>=€%"


for /f "usebackq tokens=* delims=^" %%a in ("%FILE_IN%") do (
	set /a COUNT+=2
	set "LINE_!COUNT!=%%a"
	call :ECHO_LINE LINE_!COUNT! "%FILE_IN%" "%FILE_OUT%"
)

::::::::::::::::::::::::::::
::::::::::::::::::::::::::::

if .!INSERT!==.yes (
	set /a COUNT=!COUNT!+1
	set "LINE_%N_IN%=%SEARCH_THIS%"

	set /a LINE_MODULO=N_IN%%2
	set /a LINE_SHOW_N=%N_IN%/2+!LINE_MODULO!
	REM echo  %N_IN% / 2 + !LINE_MODULO! == !LINE_SHOW_N!

	set "FILE_IN_BACKUP=%FILE_IN_PATH%%FILE_IN_NAME%.ORI"

	copy /y "%FILE_IN%" /b "!FILE_IN_BACKUP!" /b 1>nul
	if not %errorlevel% EQU 0 (
		echo\
		echo 	Copia di "%FILE_IN%"
		echo 	      in "!FILE_IN_BACKUP!"
		echo 	Copia non riuscita
		echo 	controlla i permessi
		echo\
		goto :END_00
	)
	
	REM empty file
	@echo off>"%FILE_OUT%"
	for %%G in ("%FILE_OUT%") do (
		set "FILE_OUT_SIZE=%%~zG"
	)
	REM echo -%FILE_IN_SIZE%-%FILE_OUT_SIZE%-!FILE_OUT_SIZE!-
	if !FILE_OUT_SIZE! GTR 0 (
		echo\
		echo 	Impossibile modificare il file
		echo 	"%FILE_OUT%"
		echo 	controlla i permessi
		echo\
		goto :END_00
	)

	for /L %%M in (0,1,!COUNT!) do (
		if defined LINE_%%M (
			echo\!LINE_%%M!>>"%FILE_OUT%"
		)
	)

	set "LINE_IN_SHOW_HERE=!LINE_%N_IN%!"
	set "LINE_IN_SHOW_HERE=!LINE_IN_SHOW_HERE:~8!"
	set "LINE_IN_SHOW_HERE=!LINE_IN_SHOW_HERE:~0,85!"

	set "INSERT_BEFORE_THIS_SHOW_HERE=%INSERT_BEFORE_THIS%"
	set "INSERT_BEFORE_THIS_SHOW_HERE=!INSERT_BEFORE_THIS_SHOW_HERE:~4!"

	echo\
	echo 	nel file %FILE_IN%
	echo 	ho inserito la riga "!LINE_SHOW_N!"
	echo\"!LINE_IN_SHOW_HERE!... .. .  .   .continua"
	echo 	prima della riga
	echo\"!INSERT_BEFORE_THIS_SHOW_HERE!"
	echo\
	echo 	.......il file originale "%FILE_IN%"
	echo 	è stato copiato nel file "!FILE_IN_BACKUP!"
	echo\

	tasklist /FI "IMAGENAME eq notepad++.exe" | findstr notepad++.exe >nul
	if !errorlevel! EQU 0 (
		echo 	per rendere effettive le modifiche
		echo 	chiudi e riapri notepad++
		echo\
	) else (
		echo 	apri notedad++ per attivare le modifiche
		echo\
	)

	echo\>>"!FILE_IN_BACKUP!"
	echo\^<^^!-- >>"!FILE_IN_BACKUP!"
	echo %date% - %time% >>"!FILE_IN_BACKUP!"
	echo   Questo file >>"!FILE_IN_BACKUP!"
	echo !FILE_IN_BACKUP! >>"!FILE_IN_BACKUP!"
	echo   è la copia di >>"!FILE_IN_BACKUP!"
	echo %FILE_IN% >>"!FILE_IN_BACKUP!"
	echo   A cui è stata aggiunta la riga "!LINE_SHOW_N!">>"!FILE_IN_BACKUP!"
	echo "!LINE_IN_SHOW_HERE!... .. .  .   .continua">>"!FILE_IN_BACKUP!"
	echo   prima della riga>>"!FILE_IN_BACKUP!"
	echo "!INSERT_BEFORE_THIS_SHOW_HERE!">>"!FILE_IN_BACKUP!"
	echo\>>"!FILE_IN_BACKUP!"
	echo   Per ripristinare questo file, rimuovi questo commento >>"!FILE_IN_BACKUP!"
	echo   e copia questo file >>"!FILE_IN_BACKUP!"
	echo "!FILE_IN_BACKUP!" >>"!FILE_IN_BACKUP!"
	echo   nel file >>"!FILE_IN_BACKUP!"
	echo "%FILE_IN%" >>"!FILE_IN_BACKUP!"
	echo\--^>>>"!FILE_IN_BACKUP!">>"!FILE_IN_BACKUP!"

) else (
	if .!INSERT!==.no (
		echo\
		echo 	il file "%FILE_IN%"
		echo  	non é stato modificato.
		echo\
	)
)

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:ECHO_LINE
::::  aspetto 
::::  %1 == LINE_!COUNT!
::::  %2 == "%FILE_IN%"
::::  %3 == "%FILE_OUT%"
::::  call :ECHO_LINE LINE_!COUNT! "%FILE_IN%" "%FILE_OUT%"
if not [%0]==[:ECHO_LINE] goto :ECHO_LINE_END

if not defined FILE_IN_PATH set "FILE_IN_PATH=%~dp2"
if not defined FILE_IN_NAME set "FILE_IN_NAME=%~nx2"
if not defined FILE_IN_SIZE set "FILE_IN_SIZE=%~z2"
if not defined FILE_OUT_SIZE set "FILE_OUT_SIZE=%~z3"
if not defined INSERT set INSERT=yes

	set "QQ=!%1!"
	set "KK=%QQ:<=§%"
	set "KK=%KK:>=€%"
	set "KK=%KK:"='%"
	set "KK=%KK:&=#%"
	set "KK=%KK:;=ç%"
	set "KK=%KK:!=±%"

	if ["%KK%"]==["%SEARCH_Q%"] (
		set INSERT=no
	) else (
		set TEST_INSERT=yes&    REM  Variabile non usata
	)

	if ["%KK%"]==["%INSERT_BEFORE_Q%"] set /a N_IN=%COUNT%-1

if [%0]==[:ECHO_LINE] goto :EOF
:ECHO_LINE_END


:END_00
echo  %~f0  start %START_TIME%
echo  %~f0    end %date% - %time%
echo //////////////////////////
endlocal
goto :EOF